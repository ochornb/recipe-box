# Recipe Box
- - -
based on the project by [Edd Yerburgh](https://codepen.io/eddyerburgh/pen/xVeJvB)
## View it [here](https://ochornb.bitbucket.io/recipebox/dist/index.html)
- - - -

[![Image of Recipe Box](https://bitbucket.org/ochornb/ochornb.bitbucket.io/raw/16ef985a651686a74b969db49a873ef97240078e/portfolio/imgs/recipebox.png)](https://ochornb.bitbucket.io/recipebox/dist/index.html)
## Tools used
- - -
* HTML, CSS, JavaScript
* Sass
* Bootstrap
* Git / Bitbucket
* jQuery



## Summary
- - -
I made this project to get better at using Bootstrap and Javascript and make use of Bootstrap's responsive classes and learn a bit of jQuery.
On this webpage the user can click on recipe cards to view the ingredients and instructions, edit and delete the recipe, and also
allow the user to add new recipes.

Editing the recipe swaps out the HTML from a paragraph to input fields. When the user saves or exits the modal, the values in the input fields
are saved and the input fields are reverted back to HTML and the text from the inputs are displayed.

## Problems faced and solutions for them
- - -
**Cards not the same height:**
    Some of the images were not the same height and were causing problems, so I thought limiting the height of the images would fix it.
    It did not, created more problems. Then I found an answer online using flex align-stretch to make the cards evenly sized! The images
    are not aligned but that's okay.

**Text formatting:**
    When I first had the recipe ingredients and instructions written, I tried to format the default recipes using escaped newline characters.
    This didn't format into newlines, reason: it was being put into HTML. Fix, use <br> tags.
    This gave the problem of showing break tags in the text boxes when you edited the recipe, too. Fix: use Regex to swap out <br> for newlines
    and vis versa when the recipe is saved.